function NovaOpcao(ul,select,srcImg,valueSelect,textSelect,result){
	var li, div, img, option;
  var totalOpcoes = select.find('option').length;
	li = $('<li />',{
  	id:totalOpcoes
  });
  
  div = $('<div />').click(function(){
    select.find('option').eq(totalOpcoes).prop('selected',true);
  	result.text(select.find('option').eq(totalOpcoes).val());
  });
  
  img = $('<img />',{
  	'src':srcImg,
  });
  
  option = $('<option />',{
  	'value':valueSelect,
  }).text(textSelect);
  
  img.appendTo(div);
  div.appendTo(li);
  li.appendTo(ul);
  
  option.appendTo(select);
}

$(function(){
		var ul = $('#listaImagens'),
    		select = $('#selectDisabled'),
        result = $('#result');
        
   	var imgs = new Array();
    
    imgs.push({
    	'url':'https://3.bp.blogspot.com/--Ao1YhjwpXo/V76Z4KAb_fI/AAAAAAAAFg0/fWIGVFq5KfUNooR1H3d5LUZwCCEosj8XwCLcB/s1600/uwgsnbe.png',
      'value':'opcao 1',
      'text':'1'
    });
    
    imgs.push({
    	'url':'http://cdn2.colorir.com/desenhos/pintar/numero-2_2.png',
      'value':'opcao 2',
      'text':'2'
    });
    
    imgs.push({
    	'url':'http://blog.elabogado.com/wp-content/uploads/2015/02/3.png',
      'value':'opcao 3',
      'text':'3'
    });
    
    NovaOpcao(ul,select,imgs[0]['url'],imgs[0]['value'],imgs[0]['text'],result);
    NovaOpcao(ul,select,imgs[1]['url'],imgs[1]['value'],imgs[1]['text'],result);
    NovaOpcao(ul,select,imgs[2]['url'],imgs[2]['value'],imgs[2]['text'],result);
});