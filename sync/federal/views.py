from django.shortcuts import render
from django.db import connection
from models import *
from datetime import date
import os
import commands
import csv
from unicodedata import normalize
import os.path
import zipfile
from xml.etree import ElementTree

def cadastraCotaFederal(request):

	#os.system("wget --directory-prefix=federal/xml http://www.camara.gov.br/cotas/AnoAtual.zip")
	arquivoZip = zipfile.ZipFile('federal/xml/AnoAtual.zip', 'r')
	arquivoZip.extractall('federal/xml')
	numeroArquivos = splitFile("federal/xml/AnoAtual.xml")
	nomeArquivo = 'AnoAtual'
	SQL = connection.cursor()
	jaExiste = False
	idCategoria = 0
	idFederal = 0

	#para cada arquivo gerado, ele abre e le os dados
	for i in range(1, numeroArquivos+1):
		pastaArquivo = 'federal/xml/'+nomeArquivo+str(i)+'.xml'
		with open(os.path.join(pastaArquivo), 'rt') as arquivo:
			xml_tree = ElementTree.parse(arquivo).getroot()

		for despesa_xml in xml_tree.getiterator("DESPESA"):
			try:
				nome = despesa_xml.find("txNomeParlamentar").text
				partido = despesa_xml.find("sgPartido").text
				estado = despesa_xml.find("sgUF").text
				categoria = despesa_xml.find("txtDescricao").text
				ano = despesa_xml.find("numAno").text 
				mes = despesa_xml.find("numMes").text
				gasto = despesa_xml.find("vlrLiquido").text
				valor = float(gasto.replace(',','.'))
				documento = despesa_xml.find("ideDocumento").text
			except AttributeError:
				continue

			if estado == 'ES':
				for f in DeputadosFederais.objects.raw("SELECT * FROM federal_DeputadosFederais"):
					if f.nome == nome:
						print "ja existe"
						jaExiste = True
				if jaExiste == False:
					SQL.execute("INSERT INTO federal_DeputadosFederais (nome, partido, imagem) VALUES ('%s', '%s', '%s')" % (nome, partido, 'default.png'))
			
			jaExiste = False
			for c in Categorias.objects.raw("SELECT * FROM federal_Categorias"):
				if c.nome == categoria:
					jaExiste = True
			if jaExiste == False:
				SQL.execute("INSERT INTO federal_Categorias (nome) VALUES ('%s')" % (categoria))

			#pega o id de categoria e do senador
			jaExiste = False

			for f in DeputadosFederais.objects.raw("SELECT * FROM federal_DeputadosFederais"):
				if f.nome == nome:
					for c in Categorias.objects.raw("SELECT * FROM federal_Categorias"):
						if c.nome == categoria:
							idFederal = f.id
							idCategoria = c.id
							for g in GastosCategoriaFederal.objects.raw("SELECT * FROM federal_GastosCategoriaFederal"):
								if g.gasto == valor and g.ano == ano and g.mes == mes and g.documento == documento:
									jaExiste = True
							if jaExiste == False:
								SQL.execute("INSERT INTO federal_GastosCategoriaFederal (federal_id, categoria_id, gasto, ano, mes, documento) VALUES (%d, %d, %f, '%s', '%s', '%s')" % (idFederal, idCategoria, valor, ano, mes, documento))
			jaExiste = False
					
	arquivoZip.close()
	return render(request, 'cadastraSenador.xhtml',{})	

def splitFile(nameFile):
	w = '<\0/\0D\0E\0S\0P\0E\0S\0A\0S\0>\0<\0/\0o\0r\0g\0a\0o\0>\0'
	v = '<\0o\0r\0g\0a\0o\0>\0<\0D\0E\0S\0P\0E\0S\0A\0S\0>\0'
	i=0
	n=1
	nameF = nameFile[0:len(nameFile)-4]
	name = nameF+str(n)+'.xml'	
	with open(nameFile) as f:
		print 'Criando arquivo',name
		newFile = open(name, 'w')
		while True:
			l = f.read(2)
			if not l:
				print "Fim do arquivo"
				break
			newFile.write(l)
			i = i+1
			if(i==20000000):
				i=0
				while True:
					l = f.read(2)
					if not l:
						break
					if(l == '/\0' ):
						newFile.write(l)
						l = f.read(2)
						if(l == 'D\0' ):
							newFile.write(l)
							l = f.read(2)
							if(l == 'E\0' ):
								newFile.write(l)
								for k in range(0,6):
									l = f.read(2)
									newFile.write(l)
								newFile.write(w)
								break
					newFile.write(l)
				newFile.close()
				n=n+1
				name = nameF+str(n)+'.xml'
				newFile = open(name, 'w')
				print 'Criando arquivo',n
				newFile.write(v)
		newFile.close()
	os.remove(os.path.join(nameFile))
	return n