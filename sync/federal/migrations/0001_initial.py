# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Categorias',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=500)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DeputadosFederais',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=100)),
                ('partido', models.CharField(default=9, max_length=100)),
                ('imagem', models.CharField(default=b'default.png', max_length=500)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GastosCategoriaFederal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('gasto', models.FloatField(max_length=20)),
                ('ano', models.CharField(default=2016, max_length=20)),
                ('mes', models.CharField(default=9, max_length=20)),
                ('data', models.CharField(default=9, max_length=20)),
                ('documento', models.CharField(default=9, max_length=30)),
                ('categoria', models.ForeignKey(to='federal.Categorias')),
                ('federal', models.ForeignKey(to='federal.DeputadosFederais')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ImagemCategoriaFederal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=200)),
                ('imagem', models.CharField(default=b'default.png', max_length=500)),
                ('categoria', models.ForeignKey(to='federal.Categorias')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
