from django.conf.urls import include, url
from . import views

urlpatterns = [
    
    url(r'^cadastraCota/$', views.cadastraCota),
    url(r'^cadastraCotaAnosAnteriores/$', views.cadastraCotaAnosAnteriores),
    url(r'^cadastraSenador/$', views.cadastraSenador),
    url(r'^totalGastos/$', views.totalGastos),
    url(r'^gastoSenador/$', views.gastoSenador),
    url(r'^imagensCategoriaSenado/$', views.imagensCategoriaSenado),
 ]