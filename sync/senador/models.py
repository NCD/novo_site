from django.db import models

class Senadores(models.Model):
	nome = models.CharField(max_length=100)
	partido = models.CharField(max_length=100, default = 9)
	imagem = models.CharField(max_length=500, default= 'default.png')
	
class Categorias(models.Model):
	nome = models.CharField(max_length=500)
	
class GastosCategoriaSenador(models.Model):
	senador = models.ForeignKey(Senadores)
	categoria = models.ForeignKey(Categorias)
	gasto = models.FloatField(max_length=20)
	ano = models.CharField(max_length=20, default=2016)
	mes = models.CharField(max_length=20, default = 9)
	data = models.CharField(max_length=20, default = 9)
	documento = models.CharField(max_length=30, default = 9)

class ImagemCategoriaSenado(models.Model):
	nome = models.CharField(max_length=200)
	imagem = models.CharField(max_length=500, default= 'default.png')
	categoria = models.ForeignKey(Categorias)