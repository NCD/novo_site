# -*- coding: iso-8859-1 -*-
from django.shortcuts import render
from django.db import connection
from models import *
from datetime import date
from forms import *
import os
import commands
import csv
from unicodedata import normalize
import os.path

fieldnamesCota = ['ANO', 'MES', 'SENADOR', 'TIPO_DESPESA', 'CNPJ_CPF', 'FORNECEDOR', 'DOCUMENTO', 'DATA', 'DETALHAMENTO', 'VALOR_REEMBOLSADO']

fieldnamesSenadores = ['NOME', 'ESTADO']

def cadastraSenador(request):
	#Baixando o arquivo csv e abrindo o arquivo
	os.system("wget --directory-prefix=senador/csv/senadores http://www.senado.gov.br/transparencia/lai/secrh/senador_auxilio_imoveis_csv.csv")
	csv_file = csv.DictReader(open('senador/csv/senadores/senador_auxilio_imoveis_csv.csv', 'rb'), fieldnames=fieldnamesSenadores, delimiter=';')
	csv_file.next()
	SQL = connection.cursor()

	nomeAuxiliar = None
	FLAG = False
	jaExiste = False

	for s in Senadores.objects.raw("SELECT * FROM senador_Senadores"):
		if s.nome == 'Ana Rita':
			print "ja existe"
			jaExiste = True
	#Se nao existe no banco, insere no banco o novo senador
	if jaExiste == False:
		partido = 'PT'
		SQL.execute("INSERT INTO senador_Senadores (nome, partido, imagem) VALUES ('%s', '%s', '%s')" % ('Ana Rita', partido, 'default.png'))
	
	#Percorre cada linha do arquivo csv
	for linha in csv_file:
		if linha['NOME'] != 'NOME':
			FLAG = True
		jaExiste = False
		if FLAG:
			#Verfica se o senador e do ES
			if linha['ESTADO'] == 'ES':
				nome = unicode(linha['NOME'], encoding='iso-8859-1')
				nomeCerto = nome.lower().title()
				#Procura o nome do senador no banco de dados para saber se ja existe
				for s in Senadores.objects.raw("SELECT * FROM senador_Senadores"):
					if s.nome == nomeCerto:
						print "ja existe"
						jaExiste = True
				#Se nao existe no banco, insere no banco o novo senador
				if jaExiste == False:
					#Como nao possui o partido no arquivo, e preciso colocar
					if unicode(linha['NOME'], encoding='iso-8859-1') == 'MAGNO MALTA':
						partido = 'PR'
					elif unicode(linha['NOME'], encoding='iso-8859-1') == 'ROSE DE FREITAS':
						partido = 'PMDB'
					else:
						partido = 'PSDB'
					SQL.execute("INSERT INTO senador_Senadores (nome, partido, imagem) VALUES ('%s', '%s', '%s')" % (nomeCerto, partido, 'default.png'))
					print linha['NOME']

	#Atualiza os nomes das imagens
	for s in Senadores.objects.raw("SELECT * FROM senador_Senadores"):
		nome = unicode(s.nome)
		nomeSemCaracteresEspeciais = normalize('NFKD', nome).encode('ASCII', 'ignore').decode('ASCII')
		nomeMinusculo = nomeSemCaracteresEspeciais.lower()
		nomeSemVirgula = nomeMinusculo.replace(',','')
		imagem = nomeSemVirgula.replace(' ','_')+'.png'
		pasta = 'admin/media/'+imagem

		#verifica se o arquivo existe e atualiza os nomes das imagens
		if os.path.exists(pasta) == True:
			SQL.execute("UPDATE senador_Senadores SET imagem = '%s' WHERE nome == '%s'" % (imagem, s.nome))
		else:
			SQL.execute("UPDATE senador_Senadores SET imagem = '%s' WHERE nome == '%s'" % ('default.png', s.nome))
	return render(request, 'cadastraSenador.xhtml',{})	

def cadastraCotaAnosAnteriores(request):
	ano = 2008
	alterar = 0

	while (ano < 2016):
		# link = 'http://www.senado.gov.br/transparencia/LAI/verba/'+str(ano)+'.csv'
		# comando = "wget --directory-prefix=senador/csv/cotas %s" % (link)
		# os.system(comando)
		pasta = 'senador/csv/cotas/'+str(ano)+'.csv'
		csv_file = csv.DictReader(open(pasta, 'rb'), fieldnames=fieldnamesCota, delimiter=';')
		csv_file.next()
		SQL = connection.cursor()
		 
		idSenador = 0
		idCategoria = 0
		FLAG = False
		jaExiste = False
		
		for linha in csv_file:
			idSenador = 0
			jaExiste = False

			if linha['VALOR_REEMBOLSADO'] != 'VALOR_REEMBOLSADO' and linha['VALOR_REEMBOLSADO'] != None:
				FLAG = True
			
			if FLAG:
				a = str(unicode(linha['ANO'], encoding='iso-8859-1'))
				m = str(unicode(linha['MES'], encoding='iso-8859-1'))
				d = ''
				doc = ''

				if alterar == 1:
					valor = 675.55
					alterar = 0
				elif linha['VALOR_REEMBOLSADO'] != None:
					gasto = linha['VALOR_REEMBOLSADO']
					valor = float(gasto.replace(',','.'))

				for c in Categorias.objects.raw("SELECT * FROM senador_Categorias"):
					if c.nome == unicode(linha['TIPO_DESPESA'], encoding='iso-8859-1'):
						jaExiste = True
				if jaExiste == False:
					SQL.execute("INSERT INTO senador_Categorias (nome) VALUES ('%s')" % (unicode(linha['TIPO_DESPESA'], encoding='iso-8859-1')))
					print linha['TIPO_DESPESA']
				
				jaExiste = False
				for s in Senadores.objects.raw("SELECT * FROM senador_Senadores"):
					if s.nome.upper() == unicode(linha['SENADOR'], encoding='iso-8859-1'):
						for c in Categorias.objects.raw("SELECT * FROM senador_Categorias"):
							if c.nome == unicode(linha['TIPO_DESPESA'], encoding='iso-8859-1'):
								idSenador = s.id
								idCategoria = c.id
								SQL.execute("INSERT INTO senador_GastosCategoriaSenador (senador_id, categoria_id, gasto, ano, mes, data, documento) VALUES (%d, %d, %f, '%s', '%s', '%s', '%s')" % (idSenador, idCategoria, valor, a, m, d, doc))
								print 'nao existe'
			if ano == 2013 and linha['VALOR_REEMBOLSADO'] == '255,47':
				alterar = 1
		ano = ano + 1
	return render(request, 'cadastraCotaAnosAnteriores.xhtml',{})

def cadastraCota(request):
	ano = 2016

	while (ano <= date.today().year):
		# link = 'http://www.senado.gov.br/transparencia/LAI/verba/'+str(ano)+'.csv'
		# comando = "wget --directory-prefix=senador/csv/cotas %s" % (link)
		# os.system(comando)
		pasta = 'senador/csv/cotas/'+str(ano)+'.csv'
		csv_file = csv.DictReader(open(pasta, 'rb'), fieldnames=fieldnamesCota, delimiter=';')
		csv_file.next()
		SQL = connection.cursor()
	 
		idSenador = 0
		idCategoria = 0
		FLAG = False
		jaExiste = False
		
		for linha in csv_file:
			idSenador = 0
			jaExiste = False

			if linha['VALOR_REEMBOLSADO'] != 'VALOR_REEMBOLSADO':
				FLAG = True
			
			if FLAG:
				a = str(unicode(linha['ANO'], encoding='iso-8859-1'))
				m = str(unicode(linha['MES'], encoding='iso-8859-1'))
				d = linha['DATA']
				doc = linha['DOCUMENTO']

				if linha['VALOR_REEMBOLSADO'] != None:
					gasto = linha['VALOR_REEMBOLSADO']
					valor = float(gasto.replace(',','.'))

				for c in Categorias.objects.raw("SELECT * FROM senador_Categorias"):
					if c.nome == unicode(linha['TIPO_DESPESA'], encoding='iso-8859-1'):
						jaExiste = True
				if jaExiste == False:
					SQL.execute("INSERT INTO senador_Categorias (nome) VALUES ('%s')" % (unicode(linha['TIPO_DESPESA'], encoding='iso-8859-1')))
					print linha['TIPO_DESPESA']
				
				jaExiste = False
				for s in Senadores.objects.raw("SELECT * FROM senador_Senadores"):
					if s.nome.upper() == unicode(linha['SENADOR'], encoding='iso-8859-1'):
						for c in Categorias.objects.raw("SELECT * FROM senador_Categorias"):
							if c.nome == unicode(linha['TIPO_DESPESA'], encoding='iso-8859-1'):
								idSenador = s.id
								idCategoria = c.id
								for g in GastosCategoriaSenador.objects.raw("SELECT * FROM senador_GastosCategoriaSenador"):
									if g.gasto == valor and g.mes == m and g.ano == a and g.senador_id == idSenador and g.categoria_id == idCategoria and g.data == d and g.documento == doc:
										jaExiste = True
								if jaExiste == False:
									SQL.execute("INSERT INTO senador_GastosCategoriaSenador (senador_id, categoria_id, gasto, ano, mes, data, documento) VALUES (%d, %d, %f, '%s', '%s', '%s', '%s')" % (idSenador, idCategoria, valor, a, m, d, doc))
									print 'nao existe'
		ano = ano + 1
	return render(request, 'cadastraCota.xhtml',{})

def totalGastos(request):
	gastos = GastosCategoriaSenador.objects.raw("SELECT * FROM senador_GastosCategoriaSenador")

	total = 0
	for g in gastos:
		total = total + g.gasto

	return render(request, 'totalGastos.xhtml',{'gastos':gastos, 'total':total})
	
#acrescentar menu com imagens
def gastoSenador(request):
	categoria = '0'
	mes = 0
	ano = date.today().year
	idCategoria = 0

	if request.method == "POST":
		form = FiltroForm(request.POST)
		if form.is_valid():
			ano = form.cleaned_data['ano']
			mes = form.cleaned_data['mes']
			categoria = form.cleaned_data['categoria']
	else:
		form = FiltroForm()

	if categoria == '0':
		if mes == 0:
			gastos = GastosCategoriaSenador.objects.raw("SELECT g.id, s.nome, s.partido, s.imagem, sum(g.gasto) as soma FROM senador_GastosCategoriaSenador g INNER JOIN senador_senadores s ON  g.senador_id=s.id WHERE ano == '%s' group by g.senador_id order by soma desc" % (ano))
			partidoMaiorGasto = GastosCategoriaSenador.objects.raw("SELECT g.id, s.partido, sum(g.gasto) as soma FROM senador_GastosCategoriaSenador g INNER JOIN senador_Senadores s ON  g.senador_id=s.id WHERE ano == '%s' group by s.partido order by soma desc" % (ano))
		else:
			gastos = GastosCategoriaSenador.objects.raw("SELECT g.id, s.nome, s.partido, s.imagem, sum(g.gasto) as soma FROM senador_GastosCategoriaSenador g INNER JOIN senador_senadores s ON  g.senador_id=s.id WHERE ano == '%s' and mes == '%s' group by g.senador_id order by soma desc" % (ano, mes))
			partidoMaiorGasto = GastosCategoriaSenador.objects.raw("SELECT g.id, s.partido, sum(g.gasto) as soma FROM senador_GastosCategoriaSenador g INNER JOIN senador_Senadores s ON  g.senador_id=s.id WHERE ano == '%s' and mes == '%s' group by s.partido order by soma desc" % (ano, mes))
	else:
		for c in Categorias.objects.raw("SELECT * FROM senador_Categorias"):
			if c.nome == categoria:
				idCategoria = c.id
		if mes == 0:
			gastos = GastosCategoriaSenador.objects.raw("SELECT g.id, s.nome, s.partido, s.imagem, sum(g.gasto) as soma FROM senador_GastosCategoriaSenador g INNER JOIN senador_senadores s ON  g.senador_id=s.id WHERE ano == '%s' and categoria_id == %d group by g.senador_id order by soma desc" % (ano, idCategoria))
			partidoMaiorGasto = GastosCategoriaSenador.objects.raw("SELECT g.id, s.partido, sum(g.gasto) as soma FROM senador_GastosCategoriaSenador g INNER JOIN senador_Senadores s ON  g.senador_id=s.id WHERE ano == '%s' and categoria_id == %d group by s.partido order by soma desc" % (ano, idCategoria))
		else:
			gastos = GastosCategoriaSenador.objects.raw("SELECT g.id, s.nome, s.partido, s.imagem, sum(g.gasto) as soma FROM senador_GastosCategoriaSenador g INNER JOIN senador_senadores s ON  g.senador_id=s.id WHERE ano == '%s' and mes == '%s' and categoria_id == %d group by g.senador_id order by soma desc" % (ano, mes, idCategoria))
			partidoMaiorGasto = GastosCategoriaSenador.objects.raw("SELECT g.id, s.partido, sum(g.gasto) as soma FROM senador_GastosCategoriaSenador g INNER JOIN senador_Senadores s ON  g.senador_id=s.id WHERE ano == '%s' and mes == '%s' and categoria_id == %d group by s.partido order by soma desc" % (ano, mes, idCategoria))
	
	categoriaMaiorGasto = GastosCategoriaSenador.objects.raw("SELECT g.id, c.nome, c.imagem, sum(g.gasto) as soma FROM senador_GastosCategoriaSenador g INNER JOIN senador_ImagemCategoriaSenado c ON  g.categoria_id=c.categoria_id WHERE ano == '%s' group by g.categoria_id order by soma desc" % (ano))
	
	return render(request, 'gastoSenador.xhtml',{'gastos':gastos, 'form':form, 'categoriaMaiorGasto':categoriaMaiorGasto, 'partidoMaiorGasto':partidoMaiorGasto})

def imagensCategoriaSenado(request):
	SQL = connection.cursor()
	
	for c in Categorias.objects.raw("SELECT * FROM senador_Categorias"):
		jaExiste = False
		nome = unicode(c.nome)

		if nome == unicode('Contratação de consultorias, assessorias, pesquisas, trabalhos técnicos e outros serviços de apoio ao exercício do mandato parlamentar', encoding='UTF-8'):
			for i in ImagemCategoriaSenado.objects.raw("SELECT * FROM senador_ImagemCategoriaSenado"):
				if i.nome == unicode('Consultorias, assessorias e pesquisas', encoding='UTF-8'):
					jaExiste = True
			if jaExiste == False:
				SQL.execute("INSERT INTO senador_ImagemCategoriaSenado (nome, categoria_id, imagem) VALUES ('%s', %d, '%s')" % ('Consultorias, assessorias e pesquisas', c.id, 'default.png'))
		elif nome == unicode('Locomoção, hospedagem, alimentação, combustíveis e lubrificantes', encoding='UTF-8'):
			for i in ImagemCategoriaSenado.objects.raw("SELECT * FROM senador_ImagemCategoriaSenado"):
				if i.nome == unicode('Transporte, hospedagem e alimentação', encoding='UTF-8'):
					jaExiste = True
			if jaExiste == False:
				SQL.execute("INSERT INTO senador_ImagemCategoriaSenado (nome, categoria_id, imagem) VALUES ('%s', %d, '%s')" % ('Transporte, hospedagem e alimentação', c.id, 'default.png'))
		elif nome == unicode('Aquisição de material de consumo para uso no escritório político, inclusive aquisição ou locação de software, despesas postais, aquisição de publicações, locação de móveis e de equipamentos. ', encoding='UTF-8'):
			for i in ImagemCategoriaSenado.objects.raw("SELECT * FROM senador_ImagemCategoriaSenado"):
				if i.nome == unicode('Aquisição de material de consumo', encoding='UTF-8'):
					jaExiste = True
			if jaExiste == False:
				SQL.execute("INSERT INTO senador_ImagemCategoriaSenado (nome, categoria_id, imagem) VALUES ('%s', %d, '%s')" % ('Aquisição de material de consumo', c.id, 'default.png'))
		elif nome == unicode("Divulgação da atividade parlamentar", encoding='UTF-8'):
			for i in ImagemCategoriaSenado.objects.raw("SELECT * FROM senador_ImagemCategoriaSenado"):
				if i.nome == unicode('Divulgação da atividade parlamentar', encoding='UTF-8'):
					jaExiste = True
			if jaExiste == False:
				SQL.execute("INSERT INTO senador_ImagemCategoriaSenado (nome, categoria_id, imagem) VALUES ('%s', %d, '%s')" % ('Divulgação da atividade parlamentar', c.id, 'default.png'))
		elif nome == unicode('Aluguel de imóveis para escritório político, compreendendo despesas concernentes a eles.', encoding='UTF-8'):
			for i in ImagemCategoriaSenado.objects.raw("SELECT * FROM senador_ImagemCategoriaSenado"):
				if i.nome == unicode('Aluguel de imóveis para escritório', encoding='UTF-8'):
					jaExiste = True
			if jaExiste == False:
				SQL.execute("INSERT INTO senador_ImagemCategoriaSenado (nome, categoria_id, imagem) VALUES ('%s', %d, '%s')" % ('Aluguel de imóveis para escritório', c.id, 'default.png'))
		elif nome == unicode('Passagens aéreas, aquáticas e terrestres nacionais', encoding='UTF-8'):
			for i in ImagemCategoriaSenado.objects.raw("SELECT * FROM senador_ImagemCategoriaSenado"):
				if i.nome == unicode('Passagens aéreas e terrestres', encoding='UTF-8'):
					jaExiste = True
			if jaExiste == False:
				SQL.execute("INSERT INTO senador_ImagemCategoriaSenado (nome, categoria_id, imagem) VALUES ('%s', %d, '%s')" % ('Passagens aéreas e terrestres', c.id, 'default.png'))
		elif nome == unicode('Serviços de Segurança Privada', encoding='UTF-8'):
			for i in ImagemCategoriaSenado.objects.raw("SELECT * FROM senador_ImagemCategoriaSenado"):
				if i.nome == unicode('Serviços de Segurança Privada', encoding='UTF-8'):
					jaExiste = True
			if jaExiste == False:
				SQL.execute("INSERT INTO senador_ImagemCategoriaSenado (nome, categoria_id, imagem) VALUES ('%s', %d, '%s')" % ('Serviços de Segurança Privada', c.id, 'default.png'))

	for c in ImagemCategoriaSenado.objects.raw("SELECT * FROM senador_ImagemCategoriaSenado"):
		nome = unicode(c.nome)
		nomeSemCaracteresEspeciais = normalize('NFKD', nome).encode('ASCII', 'ignore').decode('ASCII')
		nomeMinusculo = nomeSemCaracteresEspeciais.lower()
		nomeSemVirgula = nomeMinusculo.replace(',','')
		imagem = nomeSemVirgula.replace(' ','_')+'.png'
		pasta = 'admin/media/'+imagem

		#verifica se o arquivo existe
		if os.path.exists(pasta) == True:
			SQL.execute("UPDATE senador_ImagemCategoriaSenado SET imagem = '%s' WHERE nome == '%s'" % (imagem, c.nome))
		else:
			SQL.execute("UPDATE senador_ImagemCategoriaSenado SET imagem = '%s' WHERE nome == '%s'" % ('default.png', c.nome))
	return render(request, 'imagensCategoriaSenado.xhtml',{})