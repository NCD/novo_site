# -*- coding: utf-8 -*-
from django import forms
from datetime import date
import datetime
from .models import GastosCategoriaSenador, Categorias

def anos():
	anos = []
	ano = 2008

	while (ano <= date.today().year):
		anos += [(ano, str(ano))]
		ano = ano + 1
	return anos

ANOS = []
ANOS = anos()

MESES = [
	(0, "Todos os Meses"),
	(1, "Janeiro"),
	(2, "Fevereiro"),
	(3, "Março"),
	(4, "Abril"),
	(5, "Maio"),
	(6, "Junho"),
	(7, "Julho"),
	(8, "Agosto"),
	(9, "Setembro"),
	(10, "Outubro"),
	(11, "Novembro"),
	(12, "Dezembro"),
]

CAT = [
	('0', "Total"),
	('Aluguel de imóveis para escritório político, compreendendo despesas concernentes a eles.', "Aluguel de imóveis"),
	('Aquisição de material de consumo para uso no escritório político, inclusive aquisição ou locação de software, despesas postais, aquisição de publicações, locação de móveis e de equipamentos. ', "Material de Consumo"),
	('Contratação de consultorias, assessorias, pesquisas, trabalhos técnicos e outros serviços de apoio ao exercício do mandato parlamentar', "Consultorias"),
	('Locomoção, hospedagem, alimentação, combustíveis e lubrificantes', "Locomoção e hospedagem"),
	('Passagens aéreas, aquáticas e terrestres nacionais', "Passagens"),
	('Divulgação da atividade parlamentar', "Divulgação"),
	('Serviços de Segurança Privada', "Segurança"),
]

class FiltroForm(forms.Form):
	ano = forms.IntegerField(label="Ano", widget=forms.Select(choices=ANOS, attrs={'onchange':'this.form.submit();'}), required=False, initial=datetime.date.today().year)
	mes = forms.IntegerField(label="Mes", widget=forms.Select(choices=MESES, attrs={'onchange':'this.form.submit();'}), required=False, initial="Selecione")
	categoria = forms.CharField(label="Categorias", widget=forms.Select(choices=CAT, attrs={'onchange':'this.form.submit();'}), required=False, initial="Selecione")
	render = forms.CharField(initial='chart',label="", widget=forms.HiddenInput(), required=False)
